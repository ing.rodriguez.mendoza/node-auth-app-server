//console.log('Hola desde Node.js');
const express = require('express');
const res = require('express/lib/response');
const cors = require('cors');
const path = require('path');
const { dbConection } = require('./database/config');

require('dotenv').config();

// console.log(process.env);

// Crear el servidor/aplicacion de express
const app = express();

// Base de datos
dbConection();

//Directorio Público
app.use( express.static('public') );

//midelware:

//usar cors ayuda a la protección de los datos
app.use( cors({}) );

//usar para la lectura y parseo del body
app.use( express.json() );

//rutas
app.use( '/api/auth', require('./routes/auth') );

//manejar demás rutas
app.get('*', (req, res) => {
    res.sendFile( path.resolve( __dirname, 'public/index.html' ) )
})


app.listen( process.env.PORT, () => {
    console.log(`el puerto por donde corre es ${process.env.PORT}`)
});




// app.get('/', (req, res) => {
//     console.log('petición en el /');
//     res.json({
//         ok: true,
//         msg: 'ALIANZA CAMPEON LNSV!',
//         uid: 123456
//     });
//     // res.status(404).json({
//     //     ok: true,
//     //     msg: 'ALIANZA CAMPEON LNSV!',
//     //     uid: 123456
//     // });
//     // res.send('ALIANZA CAMPEON LNSV')
// });