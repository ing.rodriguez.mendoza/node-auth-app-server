const { Router } = require('express');
const { check } = require('express-validator');
const { crearUsuario, loginUsuario, revalidarToken } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

//funcion que me permite configurar mi router
const router = Router();

//registro de usuario
router.post(
    '/new',  //ruta
    [
    //    check('name', 'El nombre es obligatorio').notEmpty(),
    //    check('email', 'El email es obligatorio').isEmail(),
    //    check('password', 'El password es obligatorio').isLength(6),
    check('name')
    .notEmpty().withMessage('El nombre es obligatorio')
    .isLength({ min: 3}).withMessage('El nombre debe contener al menos 3 caracteres'),
    check('email')
    .notEmpty().withMessage('El email es obligatorio')
    .isEmail().withMessage('Email ingresado no es valido'),
    check('password')
    .notEmpty().withMessage('La contraseña es obligatoria')
    .isLength({ min: 6 }).withMessage('La contraseña es mínimo de 6 caracteres'),
    validarCampos
    ],
    crearUsuario //enviando la funcion como una referencia (controlador)
);

//login de usuario
router.post(
    '/',  //ruta
    [
      check('email', 'El email es obligatorio').isEmail(),
      check('password', 'El password es obligatorio').isLength(6),
      validarCampos
    ] , //midelware
    loginUsuario //controlador
);

//validar y revalidar token
router.get(
    '/renew',  //ruta
    validarJWT, //midelware
    revalidarToken //controlador
);

// se puede exportar un objeto o algo por defecto
module.exports = router;