const bcrypt = require("bcryptjs/dist/bcrypt");
const { response } = require("express");
const { generateJWT } = require("../helpers/jwt");
const { validarJWT } = require("../middlewares/validar-jwt");
const Usuario = require("../models/Usuario");

const crearUsuario = async (req, res = response) => { //controlador de la ruta (callback)

    const { name, email, password } = req.body;

    try {
        
        //Verificar el email
        const usuario = await Usuario.findOne({ email });

        if (usuario) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario ya existe con ese email'
            })
        }

        // Crear usuario con el modelo
        const dbUser = new Usuario( req.body );

        // Encriptar (Hash) la contraseña
        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync( password, salt );

        // console.log( 'crearUsuario', name, email  )

        // Generar el JWT
        const token = await generateJWT(dbUser.id, name);

        // Crear el usuario de base de datos
        await dbUser.save();

        // Generar respuestas exitosa
        return res.status(201).json({
            ok: true,
            uid: dbUser.id,
            name: name,//or name por EMC6
            email: dbUser.email,
            token
        });
    } 
    catch (error) {

        // console.log('ERROR');
        // console.log(error);

        return res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el Administrador'
        });

        // // console.log( name, email, password );
        //     return res.json({
        //         ok: true,
        //         msg: 'Crear usuario /new'
        // });
    }
}

const loginUsuario = async (req, res = response) => { //controlador de la ruta (callback)

    const { email, password } = req.body;
    //console.log( email, password );

    try {
    
      const dbUser = await Usuario.findOne({ email });

      if (!dbUser) {
          return res.status(400).json({
              ok: false,
              msg: 'El correo no existe'
          })
      }

      // Confirmar si el password hace match
      const validPassword = bcrypt.compareSync(password, dbUser.password);

      if (!validPassword) {
        return res.status(400).json({
            ok: false,
            msg: 'El password no es válido'
        });
      }

      // Generar el JWT
      const token = await generateJWT(dbUser.id, dbUser.name);

      // Respuesta del servicio
      return res.json({
            ok: true,
            uid: dbUser.id,
            name: dbUser.name,
            email,
            token
      });

        
    } catch (error) {
        // console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
    
}

const revalidarToken = async (req, res = response) => { //controlador de la ruta (callback)
    
    const { uid, name } = req;

    const dbUser = await Usuario.findById(uid);

    // console.log( 'revalidarToken', uid, name  )
    
    // Generar el JWT
    const token = await generateJWT(uid, name);

    // console.log( req.uid, req.name )
    return res.json({
        ok: true,
        uid: uid,
        name,
        email: dbUser.email,
        token    
    });

}


//exportar funciones
module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}

